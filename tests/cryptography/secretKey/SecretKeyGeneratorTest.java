package tests.cryptography.secretKey;

import cryptography.secretKey.SecretKeyManager;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;

/**
 *
 */
public class SecretKeyGeneratorTest  {


    @Test(expected = IllegalArgumentException.class)
    public void constructor_algorithmIsNull_IllegalArgumentException() throws NoSuchAlgorithmException {
        new SecretKeyManager(null);
    }
    @Test(expected = NoSuchAlgorithmException.class)
    public void constructor_algorithmIsNoCorrect_IllegalArgumentException() throws NoSuchAlgorithmException {
        new SecretKeyManager("AAA");
    }
}