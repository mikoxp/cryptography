/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptography.cipher;



import java.io.File;

/**
 *
 * @author miko
 */
public interface CipherCoder {
    public void encrypt(File pattern,File encryptFile) throws Exception;
    public void decrypt(File pattern,File decryptFile) throws Exception;
}
