package cryptography.cipher.aes;

import java.io.*;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;

import cryptography.cipher.CipherCoder;

import javax.crypto.*;

/**
 * @author Michal Oles
 */
public class AESCoder implements CipherCoder {

    private static final String ALGORITHM = "AES";//    AES/CBC/PKCS5Padding
    private SecretKey secretKey;
    private Cipher cipher;
    private int aesMode;

    public AESCoder(SecretKey secretKey) throws AESCoderException {
        settingSecretKey(secretKey);
        cipherInitialization();
        aesMode = 0;

    }

    private void settingSecretKey(SecretKey secretKey) throws AESCoderException {
        if (secretKey == null) {
            throw new AESCoderException("secretKey is NULL");
        }
        if (!secretKey.getAlgorithm().equals(ALGORITHM)) {
            throw new AESCoderException("secretKey algorithm is not correct: " + secretKey.getAlgorithm());
        }
        this.secretKey = secretKey;
    }

    private void cipherInitialization() throws AESCoderException {
        try {
            cipher = Cipher.getInstance(ALGORITHM);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException noSuchException) {
            throw new AESCoderException(noSuchException.getMessage());
        }
    }

    private void initiationAESCipher() throws AESCoderException {
        try {
            cipher.init(aesMode, secretKey);
        } catch (InvalidKeyException | InvalidParameterException exception) {
            throw new AESCoderException(exception.getMessage());
        }

    }

    private byte[] codingAESTransformation(byte[] codedBytes) throws AESCoderException {
        byte[] resultBytes = null;
        try {
            resultBytes = cipher.doFinal(codedBytes);
        } catch (IllegalBlockSizeException | BadPaddingException ex) {
            throw new AESCoderException("the wrong file or key to decrypt");
        }
        return resultBytes;
    }

    private void codingAES(File codedFile, File resultFile) throws AESCoderException, IOException {
        initiationAESCipher();

        FileInputStream codedFileStream = new FileInputStream(codedFile);
        FileOutputStream resultFileStream = new FileOutputStream(resultFile);

        byte[] codedBytes = new byte[(int) codedFile.length()];
        codedFileStream.read(codedBytes);
        byte[] resultBytes = codingAESTransformation(codedBytes);
        resultFileStream.write(resultBytes);

        codedFileStream.close();
        resultFileStream.close();
    }

    @Override
    public void encrypt(File pattern, File encryptFile) throws AESCoderException, IOException {
        aesMode = Cipher.ENCRYPT_MODE;
        codingAES(pattern, encryptFile);
    }

    @Override
    public void decrypt(File pattern, File decryptFile) throws AESCoderException, IOException {
        aesMode = Cipher.DECRYPT_MODE;
        codingAES(pattern, decryptFile);
    }

    public static String getALGORITHM() {
        return ALGORITHM;
    }
}
