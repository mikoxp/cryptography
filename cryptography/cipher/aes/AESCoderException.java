/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cryptography.cipher.aes;


class AESCoderException extends Exception {

    public AESCoderException(String message) {
        super(message);
    }
}
