package cryptography.secretKey;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.NoSuchAlgorithmException;

/**
 * @author miko
 */
public final class SecretKeyManager {

    private SecretKey secretKey;
    private String currentAlgorithm;

    public SecretKeyManager(String algorithm) throws NoSuchAlgorithmException {
        if (algorithm == null || "".equals(algorithm)) {
            throw new IllegalArgumentException("algorithm is NULL OR is Empty String");
        }
        this.currentAlgorithm = algorithm;
        generateSecretKey();
    }

    public void generateSecretKey() throws NoSuchAlgorithmException {
        secretKey = KeyGenerator.getInstance(currentAlgorithm).generateKey();
    }

    public void recordSecretKeyToBinaryFile(String secretKeyFileName) throws IOException {

        FileOutputStream fileOutputStream = new FileOutputStream(secretKeyFileName);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);

        DataOutputStream recorder = new DataOutputStream(bufferedOutputStream);
        byte[] secretKeyByte = secretKey.getEncoded();
        recorder.writeInt(secretKeyByte.length);
        recorder.write(secretKeyByte);
        recorder.close();


    }

    public void loadSecretKeyFromBinaryFile(String secretKeyFileName) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(secretKeyFileName);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        byte[] secretKeyByte;
        int secretKeyByteLength = 0;

        DataInputStream loader = new DataInputStream(bufferedInputStream);
        secretKeyByteLength = loader.readInt();
        secretKeyByte = new byte[secretKeyByteLength];
        for (int i = 0; i < secretKeyByteLength; i++) {
            secretKeyByte[i] = loader.readByte();
        }
        secretKey = new SecretKeySpec(secretKeyByte, 0, secretKeyByteLength, currentAlgorithm);
        loader.close();
    }

    public SecretKey getSecretKey() {
        return secretKey;
    }
////method avaliable in Java 8
//        public String getSecretKeyToString()
//    {
//        return Base64.getEncoder().encodeToString(secretKey.getEncoded());
//    }
    public void setCurrentAlgorithm(String currentAlgorithm) {
        this.currentAlgorithm = currentAlgorithm;
        secretKey = null;
    }

}
