package cryptography;

import cryptography.cipher.aes.AESCoder;
import cryptography.secretKey.SecretKeyManager;

import javax.crypto.SecretKey;
import java.io.File;


class Main {

    public static String imageFileName = "image.jpg";
    public static String textFileName = "text.txt";
    public static String movieFileName = "movie.mp4";
    public static String databaseFileName = "database.backup";
    public static String patternPath = "src//resources//pattern//";
    public static String encryptPath = "src//resources//encrypt//";
    public static String decryptPath = "src//resources//decrypt//";
    public static String secretKeyNameFile = "src//resources//secret.key";

    public static void main(String[] args) {
        try {
            System.out.println("Run:Ok");

            SecretKeyManager secretKeyManager = new SecretKeyManager(AESCoder.getALGORITHM());
            secretKeyManager.loadSecretKeyFromBinaryFile(secretKeyNameFile);
            SecretKey secretKey = secretKeyManager.getSecretKey();
            System.out.println("Load Key:DONE");

            AESCoder aesCoder = new AESCoder(secretKey);
            System.out.println("Coder ready");

            aesCoder.encrypt(new File(patternPath + imageFileName), new File(encryptPath + imageFileName));
            aesCoder.encrypt(new File(patternPath + movieFileName), new File(encryptPath + movieFileName));
            aesCoder.encrypt(new File(patternPath + textFileName), new File(encryptPath + textFileName));
            aesCoder.encrypt(new File(patternPath + databaseFileName), new File(encryptPath + databaseFileName));
            System.out.println("Encrypt:DONE");
            aesCoder.decrypt(new File(encryptPath + imageFileName), new File(decryptPath + imageFileName));
            aesCoder.decrypt(new File(encryptPath + movieFileName), new File(decryptPath + movieFileName));
            aesCoder.decrypt(new File(encryptPath + textFileName), new File(decryptPath + textFileName));
            aesCoder.decrypt(new File(encryptPath + databaseFileName), new File(decryptPath + databaseFileName));
            System.out.print("Decrypt:DONE");
        } catch (Exception e) {
            System.err.println(e);
        }

    }

}
